package ffufm.jonathan.api.spec.handler.employee

import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployeeDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

interface EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO

    /**
     * Get all Employees by page: Returns all Employees from the system that the user has access to.
     * The Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with
     * Pagination.
     * HTTP Code 200: List of Employees
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<EmployeeEmployeeDTO>

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO
}

@Controller("employee.Employee")
class EmployeeEmployeeHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeEmployeeDatabaseHandler

    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    @RequestMapping(value = ["/employees/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeEmployeeDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Get all Employees by page: Returns all Employees from the system that the user has access to.
     * The Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with
     * Pagination.
     * HTTP Code 200: List of Employees
     */
    @RequestMapping(value = ["/employees/"], method = [RequestMethod.GET])
    suspend fun getAll(
        @RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
        page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeEmployeeDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
