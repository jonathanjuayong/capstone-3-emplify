package ffufm.jonathan.api.spec.handler.employee.implementation

import ffufm.jonathan.api.PassTestBase
import ffufm.jonathan.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.jonathan.api.spec.handler.employee.EmployeeContactDetailDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class EmployeeContactDetailDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeContactDetailRepository: EmployeeContactDetailRepository

    @Autowired
    lateinit var employeeContactDetailDatabaseHandler: EmployeeContactDetailDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        employeeContactDetailRepository.deleteAll()
    }

    @Test
    fun `test create`() = runBlocking {
        val body: EmployeeContactDetail = EmployeeContactDetail()
        employeeContactDetailDatabaseHandler.create(body.toDto())
        Unit
    }

    @Test
    fun `test getByEmployee`() = runBlocking {
        val id: Long = 0
        val maxResults: Int = 100
        val page: Int = 0
        employeeContactDetailDatabaseHandler.getByEmployee(id, maxResults, page)
        Unit
    }

    @Test
    fun `test update`() = runBlocking {
        val body: EmployeeContactDetail = EmployeeContactDetail()
        val id: Long = 0
        employeeContactDetailDatabaseHandler.update(body.toDto(), id)
        Unit
    }
}
