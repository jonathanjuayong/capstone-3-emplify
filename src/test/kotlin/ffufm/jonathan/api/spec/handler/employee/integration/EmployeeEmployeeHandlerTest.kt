package ffufm.jonathan.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.jonathan.api.PassTestBase
import ffufm.jonathan.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployee
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeEmployeeHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `test create`() {
        val body: EmployeeEmployee = EmployeeEmployee()
        mockMvc.post("/employees/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }

        }
    }

    @Test
    @WithMockUser
    fun `test getAll`() {
        mockMvc.get("/employees/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isOk() }

        }
    }

    @Test
    @WithMockUser
    fun `test update`() {
        val body: EmployeeEmployee = EmployeeEmployee()
        val id: Long = 0
        mockMvc.put("/employees/{id}/", id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }

        }
    }
}
