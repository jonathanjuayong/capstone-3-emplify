package ffufm.jonathan.api.spec.handler.employee

import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.jonathan.api.spec.dbo.employee.EmployeeContactDetailDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

interface EmployeeContactDetailDatabaseHandler {
    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    suspend fun create(body: EmployeeContactDetailDTO): EmployeeContactDetailDTO

    /**
     * Find by Employee: Finds ContactDetails by the parent Employee id
     * HTTP Code 200: List of ContactDetails items
     */
    suspend fun getByEmployee(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<EmployeeContactDetailDTO>

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO
}

@Controller("employee.ContactDetail")
class EmployeeContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeContactDetailDatabaseHandler

    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    @RequestMapping(value = ["/employees/contactdetails/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeContactDetailDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Find by Employee: Finds ContactDetails by the parent Employee id
     * HTTP Code 200: List of ContactDetails items
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/contactdetails/"], method = [RequestMethod.GET])
    suspend fun getByEmployee(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByEmployee(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/employees/contactdetails/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeContactDetailDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
