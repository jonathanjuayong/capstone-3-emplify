package ffufm.jonathan.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import javax.persistence.*
import kotlin.reflect.KClass

/**
 * Employees entity for the employee management system
 */
@Entity(name = "EmployeeEmployee")
@Table(name = "employee_employee")
data class EmployeeEmployee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * First name of the employee
     * Sample: Jonathan
     */
    @Column(
        length = 100,
        updatable = true,
        nullable = false,
        name = "first_name"
    )
    val firstName: String = "",
    /**
     * Last name of the employee
     * Sample: Juayong
     */
    @Column(
        length = 100,
        updatable = true,
        nullable = false,
        name = "last_name"
    )
    val lastName: String = "",
    /**
     * Job position of the employee
     * Sample: Backend Developer
     */
    @Column(
        length = 100,
        updatable = true,
        nullable = false,
        name = "position"
    )
    val position: String = "",
    /**
     * Email of the employee
     * Sample: jjuayong@gmail.com
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "email"
    )
    val email: String = "",
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val contactDetails: List<EmployeeContactDetail>? = mutableListOf()
) : PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>() {
    override fun toDto(): EmployeeEmployeeDTO =
        super.toDtoInternal(
            EmployeeEmployeeSerializer::class as
                    KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>,
                            EmployeeEmployeeDTO, Long>>
        )

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Employees entity for the employee management system
 */
data class EmployeeEmployeeDTO(
    val id: Long? = null,
    /**
     * First name of the employee
     * Sample: Jonathan
     */
    val firstName: String? = "",
    /**
     * Last name of the employee
     * Sample: Juayong
     */
    val lastName: String? = "",
    /**
     * Job position of the employee
     * Sample: Backend Developer
     */
    val position: String? = "",
    /**
     * Email of the employee
     * Sample: jjuayong@gmail.com
     */
    val email: String? = "",
    val contactDetails: List<EmployeeContactDetailDTO>? = null
) : PassDTO<EmployeeEmployee, Long>() {
    override fun toEntity(): EmployeeEmployee =
        super.toEntityInternal(
            EmployeeEmployeeSerializer::class as
                    KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, PassDTO<EmployeeEmployee, Long>,
                            Long>, PassDTO<EmployeeEmployee, Long>, Long>>
        )

    override fun readId(): Long? = this.id
}

@Component
class EmployeeEmployeeSerializer : PassDtoSerializer<EmployeeEmployee, EmployeeEmployeeDTO, Long>() {
    override fun toDto(entity: EmployeeEmployee): EmployeeEmployeeDTO = cycle(entity) {
        EmployeeEmployeeDTO(
            id = entity.id,
            firstName = entity.firstName,
            lastName = entity.lastName,
            position = entity.position,
            email = entity.email,
            contactDetails = entity.contactDetails?.toSafeDtos()
        )
    }

    override fun toEntity(dto: EmployeeEmployeeDTO): EmployeeEmployee = EmployeeEmployee(
        id = dto.id,
        firstName = dto.firstName ?: "",
        lastName = dto.lastName ?: "",
        position = dto.position ?: "",
        email = dto.email ?: "",
        contactDetails = dto.contactDetails?.toEntities() ?: emptyList()
    )

    override fun idDto(id: Long): EmployeeEmployeeDTO = EmployeeEmployeeDTO(
        id = id,
        firstName = null,
        lastName = null,
        position = null,
        email = null,

        )
}

@Service("employee.EmployeeEmployeeValidator")
class EmployeeEmployeeValidator : PassModelValidation<EmployeeEmployee> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployee>):
            ValidatorBuilder<EmployeeEmployee> = validatorBuilder.apply {
        konstraint(EmployeeEmployee::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployee::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployee::position) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployee::email) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
    }
}

@Service("employee.EmployeeEmployeeDTOValidator")
class EmployeeEmployeeDTOValidator : PassModelValidation<EmployeeEmployeeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployeeDTO>):
            ValidatorBuilder<EmployeeEmployeeDTO> = validatorBuilder.apply {
        konstraint(EmployeeEmployeeDTO::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployeeDTO::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployeeDTO::position) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(100)
        }
        konstraint(EmployeeEmployeeDTO::email) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
    }
}
