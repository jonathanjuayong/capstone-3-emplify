package ffufm.jonathan.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import ffufm.jonathan.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.jonathan.api.spec.dbo.employee.EmployeeContactDetailDTO
import ffufm.jonathan.api.spec.handler.employee.EmployeeContactDetailDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component("employee.EmployeeContactDetailHandler")
class EmployeeContactDetailHandlerImpl : PassDatabaseHandler<EmployeeContactDetail,
        EmployeeContactDetailRepository>(), EmployeeContactDetailDatabaseHandler {
    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    override suspend fun create(body: EmployeeContactDetailDTO): EmployeeContactDetailDTO {
        TODO("Not yet implemented")
    }

    /**
     * Find by Employee: Finds ContactDetails by the parent Employee id
     * HTTP Code 200: List of ContactDetails items
     */
    override suspend fun getByEmployee(id: Long, maxResults: Int, page: Int): Page<EmployeeContactDetailDTO> {
        TODO("Not yet implemented")
    }

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO {
        TODO("Not yet implemented")
    }

}
