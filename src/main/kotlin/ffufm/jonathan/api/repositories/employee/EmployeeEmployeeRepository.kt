package ffufm.jonathan.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployee
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeEmployeeRepository : PassRepository<EmployeeEmployee, Long> {
    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.contactDetails",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchContactDetails(pageable: Pageable): Page<EmployeeEmployee>
}
