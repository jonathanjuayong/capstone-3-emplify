package ffufm.jonathan.api.spec.handler.employee.implementation

import ffufm.jonathan.api.PassTestBase
import ffufm.jonathan.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployee
import ffufm.jonathan.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class EmployeeEmployeeDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var employeeEmployeeDatabaseHandler: EmployeeEmployeeDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `test create`() = runBlocking {
        val body: EmployeeEmployee = EmployeeEmployee()
        employeeEmployeeDatabaseHandler.create(body.toDto())
        Unit
    }

    @Test
    fun `test getAll`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0
        employeeEmployeeDatabaseHandler.getAll(maxResults, page)
        Unit
    }

    @Test
    fun `test update`() = runBlocking {
        val body: EmployeeEmployee = EmployeeEmployee()
        val id: Long = 0
        employeeEmployeeDatabaseHandler.update(body.toDto(), id)
        Unit
    }
}
