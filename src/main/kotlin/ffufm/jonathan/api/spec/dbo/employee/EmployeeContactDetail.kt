package ffufm.jonathan.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.*
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import javax.persistence.*
import kotlin.reflect.KClass

/**
 * Contact details of the employee
 */
@Entity(name = "EmployeeContactDetail")
@Table(name = "employee_contactdetail")
data class EmployeeContactDetail(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Contact detail of the employee
     * Sample: 09177548620
     */
    @Column(
        length = 30,
        updatable = true,
        nullable = false,
        name = "detail"
    )
    val detail: String = "",
    /**
     * Type of contact detail
     * Sample: mobile
     */
    @Column(
        length = 30,
        updatable = true,
        nullable = false,
        name = "type"
    )
    val type: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null
) : PassDTOModel<EmployeeContactDetail, EmployeeContactDetailDTO, Long>() {
    override fun toDto(): EmployeeContactDetailDTO =
        super.toDtoInternal(
            EmployeeContactDetailSerializer::class as
                    KClass<PassDtoSerializer<PassDTOModel<EmployeeContactDetail, EmployeeContactDetailDTO,
                            Long>, EmployeeContactDetailDTO, Long>>
        )

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Contact details of the employee
 */
data class EmployeeContactDetailDTO(
    val id: Long? = null,
    /**
     * Contact detail of the employee
     * Sample: 09177548620
     */
    val detail: String? = "",
    /**
     * Type of contact detail
     * Sample: mobile
     */
    val type: String? = "",
    val employee: EmployeeEmployeeDTO? = null
) : PassDTO<EmployeeContactDetail, Long>() {
    override fun toEntity(): EmployeeContactDetail =
        super.toEntityInternal(
            EmployeeContactDetailSerializer::class as
                    KClass<PassDtoSerializer<PassDTOModel<EmployeeContactDetail,
                            PassDTO<EmployeeContactDetail, Long>, Long>, PassDTO<EmployeeContactDetail, Long>,
                            Long>>
        )

    override fun readId(): Long? = this.id
}

@Component
class EmployeeContactDetailSerializer : PassDtoSerializer<EmployeeContactDetail,
        EmployeeContactDetailDTO, Long>() {
    override fun toDto(entity: EmployeeContactDetail): EmployeeContactDetailDTO = cycle(entity) {
        EmployeeContactDetailDTO(
            id = entity.id,
            detail = entity.detail,
            type = entity.type,
            employee = entity.employee?.idDto() ?: entity.employee?.toDto()
        )
    }

    override fun toEntity(dto: EmployeeContactDetailDTO): EmployeeContactDetail =
        EmployeeContactDetail(
            id = dto.id,
            detail = dto.detail ?: "",
            type = dto.type ?: "",
            employee = dto.employee?.toEntity()
        )

    override fun idDto(id: Long): EmployeeContactDetailDTO = EmployeeContactDetailDTO(
        id = id,
        detail = null,
        type = null,

        )
}

@Service("employee.EmployeeContactDetailValidator")
class EmployeeContactDetailValidator : PassModelValidation<EmployeeContactDetail> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContactDetail>):
            ValidatorBuilder<EmployeeContactDetail> = validatorBuilder.apply {
        konstraint(EmployeeContactDetail::detail) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(30)
        }
        konstraint(EmployeeContactDetail::type) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(30)
        }
        konstraintOnObject(EmployeeContactDetail::employee) {
            notNull()
        }
    }
}

@Service("employee.EmployeeContactDetailDTOValidator")
class EmployeeContactDetailDTOValidator : PassModelValidation<EmployeeContactDetailDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContactDetailDTO>):
            ValidatorBuilder<EmployeeContactDetailDTO> = validatorBuilder.apply {
        konstraint(EmployeeContactDetailDTO::detail) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(30)
        }
        konstraint(EmployeeContactDetailDTO::type) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(30)
        }
        konstraintOnObject(EmployeeContactDetailDTO::employee) {
            notNull()
        }
    }
}
