package ffufm.jonathan.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.jonathan.api.PassTestBase
import ffufm.jonathan.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeContactDetail
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeContactDetailHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var employeeContactDetailRepository: EmployeeContactDetailRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        employeeContactDetailRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `test create`() {
        val body: EmployeeContactDetail = EmployeeContactDetail()
        mockMvc.post("/employees/contactdetails/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }

        }
    }

    @Test
    @WithMockUser
    fun `test getByEmployee`() {
        val id: Long = 0
        mockMvc.get("/employees/{id}/contactdetails/", id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isOk() }

        }
    }

    @Test
    @WithMockUser
    fun `test update`() {
        val body: EmployeeContactDetail = EmployeeContactDetail()
        val id: Long = 0
        mockMvc.put("/employees/contactdetails/{id}/", id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }

        }
    }
}
