package ffufm.jonathan.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import ffufm.jonathan.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployee
import ffufm.jonathan.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.jonathan.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component("employee.EmployeeEmployeeHandler")
class EmployeeEmployeeHandlerImpl : PassDatabaseHandler<EmployeeEmployee,
        EmployeeEmployeeRepository>(), EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    override suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO {
        TODO("Not yet implemented")
    }

    /**
     * Get all Employees by page: Returns all Employees from the system that the user has access to.
     * The Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with
     * Pagination.
     * HTTP Code 200: List of Employees
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<EmployeeEmployeeDTO> {
        TODO("Not yet implemented")
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO {
        TODO("Not yet implemented")
    }

}
